import greenfoot.*;

/**
 * Start klassen er starten af programmet (hoved menuen).
 * 
 * @author Peter Mikkelsen
 * @version 1
 */
public class Start extends GameScreen
{
    // Opret en knap der starter et nyt aktivt spil
    private Button startButton = new Button("Start spil", () ->
    {
         Play game = new Play();
         Greenfoot.setWorld(game);
    });
    
    // Opret en knap der lukker spillet helt (greenfoot starter det igen).
    private Button quitButton = new Button("Afslut", () -> System.exit(0));
    
    public Start()
    {
        // Tilføj knapperne på skærmen
        addObject(startButton, config.SCREEN_CENTER_X, config.SCREEN_CENTER_Y - config.BUTTON_FONT_SIZE);
        addObject(quitButton, config.SCREEN_CENTER_X, config.SCREEN_CENTER_Y);
    }
}