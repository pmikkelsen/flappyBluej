import greenfoot.*;

/**
 * GameOver klassen repræsenterer et spils slutning.
 * 
 * @author Peter Mikkelsen
 * @version 1
 */
public class GameOver extends GameScreen
{
    
    // Opret en knap der går til hovedmenuen
    private Button mainMenu = new Button("Tilbage til startskærm", () ->
    {
        Start start = new Start();
        Greenfoot.setWorld(start);
    });
    
    // Opret en knap der går til et nyt aktivt spil
    private Button playAgain = new Button("Prøv igen", () ->
    {
        Play game = new Play();
        Greenfoot.setWorld(game);
    });
    
    public GameOver(int score, Bird.deathReasons reason)
    {   
        // Få en string med dødsårsagen, og print den ud lidt over midt på skærmen
        String reasonString = getReasonString(reason);
        showText("Øv, " + reasonString, config.SCREEN_CENTER_X, config.SCREEN_CENTER_Y - 50);
        
        // Print spilleren score på skærmen
        showText("Din score var " + score, config.SCREEN_CENTER_X, config.SCREEN_CENTER_Y);

        // Tilføj de 2 knapper på skærmen 
        addObject(mainMenu, config.SCREEN_CENTER_X, config.SCREEN_HEIGHT - config.BUTTON_FONT_SIZE*2);
        addObject(playAgain, config.SCREEN_CENTER_X, config.SCREEN_HEIGHT - config.BUTTON_FONT_SIZE);
    }
    
    public String getReasonString(Bird.deathReasons reason)
    {
        // Opret en string ud fra dødsårsagen, som er defineret i Bird klassen
        switch (reason) {
            case HIT_GROUND:
                return "du ramte jorden :(";
            case HIT_PIPE:
                return "du ramte et rør :(";
            default:
                // Denne string returneres kun, hvis der er tilføjet en dødsårsag
                // i Bird klassen, som der ikke er lavet kode for her.
                return "du døde af en ukendt grund :|";
        }
    }
}
