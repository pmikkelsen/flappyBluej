import greenfoot.*;
import java.lang.Runnable; // Bruges til at gemme onClick funktionen i klassen.

/**
 * Button klassen representerer en knap i spillet.
 * 
 * @author Peter Mikkelsen
 * @version 1
 */
public class Button extends Actor
{
    // onClick er den funktion som bliver kørt når der trykkes på knappen.
    private Runnable onClick;
    
    /* Button tager to argumenter.
     * text er den String som der står på knappen.
     * run er den funktion der skal køreres når knappen klikkes.
     */
    public Button(String text, Runnable run)
    {
        //Knappen vises ved et GreenfootImage med noget tekst på. 
        //Skriftstørrelsen er 50, farverne er defineret i config filen.
        GreenfootImage image = new GreenfootImage(text, 50, config.BUTTON_FG, config.BUTTON_BG, config.BUTTON_OUTLINE);
        // billedet sættes til at være knappens billede.
        setImage(image);
        // den private variabel onClick sættes til run.
        onClick = run;
    }
    
    public void act() 
    {
        // hvis musen klikker indenfor knappens billede: afspil "klik" lyd og kør onClick.
        if (Greenfoot.mouseClicked(this)) {
            Greenfoot.playSound("klik.wav");
            onClick.run();
        }
    }
}
