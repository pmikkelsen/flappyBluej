import greenfoot.*;

/**
 * PipeSegment klassen repræsenterer den synlige del af et rør.
 * 
 * @author Peter Mikkelsen
 * @version 1
 */
public class PipeSegment extends Actor
{
    // Skab en rør del
    public PipeSegment(int top, int bottom) {
        GreenfootImage img = new GreenfootImage(config.PIPE_WIDTH, bottom-top);
        img.setColor(config.PIPE_COLOR);
        img.fill();
        setImage(img);
    }
}
