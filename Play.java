import greenfoot.*;

/**
 * Play klassen repræsenterer et aktivt spil af flappyBluej
 * 
 * @author Peter Mikkelsen
 * @version 1
 */
public class Play extends GameScreen
{
    // variablen score holder styr på hvor mange point spilleren har
    private int score = 0;
    
    // bird er fuglen som der spilles med
    Bird bird = new Bird();
    
    // pipes er et array af rør, som holder alle spillets rør. Størrelsen er
    // defineret i config klassen.
    Pipe[] pipes = new Pipe[config.NUMBER_OF_PIPES];
    
    public Play()
    {  
        // Først tilføles fuglen til verdenen. 
        addObject(bird, config.BIRD_X, config.SCREEN_CENTER_Y);
        
        // Alle rørene initialiseres
        for (int i = 0; i < config.NUMBER_OF_PIPES; i++) {
            
            // En tilfældig y-værdi genereres. Den repræsenterer midten af rørets hul.
            // Den variere med 500 så spillet ikke bliver for svært.
            int y = Greenfoot.getRandomNumber(500) + (config.SCREEN_HEIGHT - 500)/2;
            pipes[i] = new Pipe(y);
            // Det nye rør tilføjes til verdenen. 
            addObject(pipes[i], config.SCREEN_WIDTH + (i+1) * ((config.SCREEN_WIDTH+config.PIPE_WIDTH) / config.NUMBER_OF_PIPES), config.SCREEN_CENTER_Y);
        }
    }
    
    public void act()
    {
        // Scoren vises i øverste venstre hjørne.
        showText("Score: " + score, 100, 50);
        
        // Hvis fuglen er død: Få dødsårsagen og afslut spillet.
        if (bird.isDead()) {
            Bird.deathReasons reason = bird.getDeathReason();
            gameOver(reason);
        }
        
        // Hvis der skal gives point: Afspil point lyd og tilføj 1 til score.
        if (bird.shouldGetPoint()) {
            Greenfoot.playSound("point.wav");
            score++;
        }
        
        // Ryk alle pipes lidt mod venstre på skærmen, så vi simulerer en bevægende verden.
        for (Pipe p : pipes) {
            p.move(-config.PIPE_SPEED);
            
            // hvis et rør har nået enden af banen rykkes den til starten.
            if (p.getX() < -config.PIPE_WIDTH/2) {
                int y = Greenfoot.getRandomNumber(500) + (config.SCREEN_HEIGHT - 500)/2;
                p.replace(config.SCREEN_WIDTH+config.PIPE_WIDTH/2, y);
            }
        }
    }
    
    private void gameOver(Bird.deathReasons reason)
    {
        // Afspil en lyd der lyder som om fuglen rammer noget
        Greenfoot.playSound("hit.wav");
        GameOver g = new GameOver(score,reason);
        
        // sæt verdnen til GameOver skærmen
        Greenfoot.setWorld(g);
    }
}
