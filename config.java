import greenfoot.*;

/**
 * Config klassen repræsenterer spillets konfiguration
 * 
 * @author Peter Mikkelsen
 * @version 2
 */
public final class config
{
    public static final String BACKGROUND = "lava.png";
    
    public static final int SCREEN_WIDTH = 1300;
    public static final int SCREEN_HEIGHT = 800;
    public static final int SCREEN_CENTER_X = SCREEN_WIDTH/2;
    public static final int SCREEN_CENTER_Y = SCREEN_HEIGHT/2;
    
    public static final Color BUTTON_FG = Color.WHITE;
    public static final Color BUTTON_BG = new Color(0,0,0,0);
    public static final Color BUTTON_OUTLINE = Color.BLACK;
    public static final int BUTTON_FONT_SIZE = 50;
    
    public static final int NUMBER_OF_PIPES = 4;
    public static final int PIPE_SPEED = 6;
    public static final int PIPE_GAP = 200;
    public static final int PIPE_WIDTH = 80;
    public static final Color PIPE_COLOR = Color.DARK_GRAY;
    
    public static final int BIRD_X = 100;
    public static final int MAX_BIRD_SPEED = 15;
    public static final double BIRD_ACCELERATION = 0.5;
    public static final int BIRD_ROTATION_DELTA = 2;
    public static final int BIRD_JUMP_SPEED = 10;
}
