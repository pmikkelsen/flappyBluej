import greenfoot.*;

/**
 * Bird klassen repræsenterer en fugl i spillet.
 * 
 * @author Peter Mikkelsen
 * @version 1
 */
public class Bird extends Actor
{
    // De forskellige dødsårsager der kan forekomme
    enum deathReasons {
        HIT_GROUND,
        HIT_PIPE,
    };
    
    // Fuglens hastighed
    private double speed = 0;

    // inJump er true hvis fuglen er i et rør.
    private boolean inPipe = false;
    
    // jumping er true hvis fuglen er i gang med at hoppe.
    private boolean jumping = false;
    
    // Variablen deathReason gemmer dødsårsagen så den kan fås senere.
    private deathReasons deathReason;
    
    public Bird()
    {
        // Fuglens billede nedskaleres til en passende størrelse.
        GreenfootImage img = getImage();
        img.scale(64, 50);
        setImage(img);
        
        // Rotationen sættes, så fuglen pejer lidt op.
        setRotation(-45);
    }
    public void act() 
    {
       // Hvis der trykkes på mellemrum, og fuglen ikke er igang med et hop
       if (Greenfoot.isKeyDown("space") && !jumping) {
           // hop (sæt en opadgående hastighed)
           speed = -config.BIRD_JUMP_SPEED;
           
           // sæt ny vinkel
           setRotation(-45);
           
           // afspil flyve lyden
           Greenfoot.playSound("flyv.wav");
           
           // da fuglen nu hopper sættes jumping til true
           jumping = true;
       } else if (Greenfoot.isKeyDown("space")) {
           // hvis 
           fallDown();
       } else {
           fallDown();
           jumping = false;
       }
       int rot = getRotation() + config.BIRD_ROTATION_DELTA;
       if (rot > 90 && rot < 270) {
           setRotation(90);
       } else {
           setRotation(rot);
       }
       
       if (speed < config.MAX_BIRD_SPEED) {
           speed += config.BIRD_ACCELERATION;
       }
    }
    
    private void fallDown()
    {
        setLocation(getX(), getY() + (int)speed);
    }
    
    public boolean isDead()
    {
        if (isAtEdge() && getY() < 100) {
            setLocation(getX(), 1);
            speed = 0;
            setRotation(-45);
        }
        if (isAtEdge() && getY() > 100) {
            deathReason = deathReasons.HIT_GROUND;
            return true;
        }
        if (isTouching(PipeSegment.class)) {
            deathReason = deathReasons.HIT_PIPE;
            return true;
        }
        return false;
    }
    
    public deathReasons getDeathReason()
    {
        return deathReason;
    }
    
    public boolean shouldGetPoint()
    {
        if (isTouching(Pipe.class) && !inPipe) {
            inPipe = true;
            return true;
        } else if (isTouching(Pipe.class)) {
            return false;
        } else {
            inPipe = false;
            return false;
        }
    }
}
