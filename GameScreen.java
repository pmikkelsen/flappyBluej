import greenfoot.*;

/**
 * GameScreen klassen er en standard skærm i spillet.
 * Den sætter størrelsen og baggrundsbilledet, så andre
 * ikke behøver at gøre det.
 * 
 * @author Peter Mikkelsen
 * @version 1
 */
public class GameScreen extends World
{

    /**
     * Constructor for objects of class GameScreen.
     * 
     */
    public GameScreen()
    {    
        // Lav en ny World med bedden og højden fra config klassen. 
        super(config.SCREEN_WIDTH, config.SCREEN_HEIGHT, 1, false);
        
        // Sæt baggrunden til værdien fra config klassen.
        setBackground(config.BACKGROUND);
    }
}
