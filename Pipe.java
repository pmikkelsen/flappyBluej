import greenfoot.*; 

/**
 * Write a description of class Pipe here.
 * 
 * @author Peter Mikkelsen 
 * @version 1
 */
public class Pipe extends Actor
{

    // Rørets y værdi (midt i rørets hul)
    private int y;
    
    // De to dele røret er opbygget af
    private PipeSegment top;
    private PipeSegment bottom;
    
    // Lav et nyt rør med en given y værdi
    public Pipe(int y)
    {
        this.y = y;
        GreenfootImage img = new GreenfootImage(1, config.SCREEN_HEIGHT);
        setImage(img);
    }
    
    // Når røret tilføjes, skabes de to rør dele
    public void addedToWorld(World w)
    {
        setupPipeSegments();
    }
    
    
    // De to rør dele skabes her
    private void setupPipeSegments()
    {
        World w = getWorld();
        top = new PipeSegment(0, y-(config.PIPE_GAP/2));
        bottom = new PipeSegment(y+(config.PIPE_GAP/2), config.SCREEN_HEIGHT);
        w.addObject(top, 0,0);
        w.addObject(bottom, 0,0);
    }
    
    // De to rør dele slettes
    private void cleanupPipeSegments()
    {
        World w = getWorld();
        w.removeObject(top);
        w.removeObject(bottom);
    }
    
    // Ved act flyttes rørdelene med røret
    public void act() 
    {
        int x = getX();
        
        top.setLocation(x, top.getImage().getHeight()/2);
        bottom.setLocation(x, getWorld().getHeight() - bottom.getImage().getHeight()/2);
    }

    
    // Her skabes et "nyt" rør ved et nyt koordinat
    public void replace(int x, int y)
    {
        this.y = y;
        
        cleanupPipeSegments();
        setupPipeSegments();
        setLocation(x, config.SCREEN_CENTER_Y);
    }
}
